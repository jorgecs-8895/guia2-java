import java.util.Scanner;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class Libro {

    public static void main(String[] args) {
	// TODO Esbozo de método generado automáticamente
	Bibliotecario bibliotecario = new Bibliotecario();
	Biblioteca libreria = new Biblioteca();
	Scanner scanner = new Scanner(System.in);

	System.out.println("Ingrese Nombre del Libro: ");
	String valor_nombre = scanner.nextLine();
	libreria.setNombreLibro(valor_nombre);

	System.out.println("Ingrese autor: ");
	String valor_autor = scanner.nextLine();
	libreria.setAutor(valor_autor);

	bibliotecario.agregar(valor_nombre, valor_autor);
	bibliotecario.visualizar();

    }

}
