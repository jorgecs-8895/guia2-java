@SuppressWarnings("unused")
public class Biblioteca {
    private String nombre_libro;
    private String autor;
    // private double ejemplares;
    // private double ejemplares_prestados;

    public Biblioteca() {
    }

    public Biblioteca(String nombre_libro, String autor) {

	this.nombre_libro = nombre_libro;
	this.autor = autor;
	// this.ejemplares = ejemplares;
	// this.ejemplares_prestados = ejemplares_prestados;
    }

    public String getNombreLibro() {
	return nombre_libro;

    }

    public void setNombreLibro(String nombre_libro) {
	this.nombre_libro = nombre_libro;
    }

    public String getAutor() {
	return autor;
    }

    public void setAutor(String autor) {
	this.autor = autor;
    }

}
