
// Importación de librería(s) y/o paquetes requeridos para el correcto funcionamiento del programa
import java.util.Scanner;

public class Tarjeta {

    @SuppressWarnings("resource")
    // Main
    public static void main(String[] args) {

	Titular cliente = new Titular();
	Scanner scaner = new Scanner(System.in);

	System.out.println("\n Bienvenid@ al Banco Santander Banefe");
	System.out.println("\n> Por favor, ingrese su nombre y apellido: ");
	String nombre_usuario = scaner.nextLine();

	// Nombre de usuario
	cliente.setNombre(nombre_usuario);

	// Nombre del banco
	cliente.setNombre_Banco("\r\n\tBanco Santander Banefe");
	System.out.println("\r\n Se ha accedido exitosamente a su cuenta." + cliente.getNombre_Banco());
	System.out.println("\r\n Bienvenid@ a su cuenta: " + cliente.getNombre());
	System.out.println("\n> Por favor, ingrese su saldo en CLP: ");

	// Ingreso y almacenamiento de dato "Saldo"
	int saldo_inicial = scaner.nextInt();
	if (saldo_inicial < 0 || saldo_inicial > 999999999) {
	    System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
	    System.exit(0);

	} else if (saldo_inicial > 0) {
	    cliente.setSaldo(saldo_inicial);

	    // Posibilidad de elegir tarjeta(s)
	    System.out.println("\r\n> Elija su tarjeta: ");
	    System.out.println("\t[1] Tarjeta Cuenta de Ahorro");
	    System.out.println("\t[2] Tarjeta de Crédito");
	    System.out.println("\t[3] Tarjeta de Débito");

	    // Scanner de Tarjetas
	    Scanner scan_tarjetas = new Scanner(System.in);
	    int option = scan_tarjetas.nextInt();
	    switch (option) {

	    // Casos luego de escoger tarjeta
	    /*
	     * Case 1.- Tarjeta Cuenta de Ahorro. Case 2.- Tarjeta de Crédito. Case 3.-
	     * Tarjeta de Débito.
	     */
	    // El usuario ingresa a Tarjeta Cuenta de Ahorro.
	    case 1:
		System.out.println("\r\n\t***** Ha ingresado exitosamente a Tarjeta Cuenta de Ahorro. *****");
		System.out.println("\n ~ Su saldo es de: " + cliente.getSaldo() + " CLP. ~");
		System.out.println("\n> ¿Qué desea hacer?");
		System.out.println("\t[1] Depositar dinero.");
		System.out.println("\t[2] Retirar dinero.");
		Scanner deposito_retiro = new Scanner(System.in);
		int alternativa = deposito_retiro.nextInt();
		// Si el usuario desea aumentar el monto final
		if (alternativa == 1) {
		    System.out.println("\n> ¿Cuánto desea depositar?");
		    Scanner aumento = new Scanner(System.in);
		    int opcion_aumento = aumento.nextInt();
		    int saldo_final = cliente.getSaldo() + opcion_aumento;
		    System.out.println("\r\n > Su nuevo saldo es de: " + saldo_final + " CLP.");
		    System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");

		}
		// Si el usuario desea disminuir el monto final
		else if (alternativa == 2) {
		    System.out.println("\n> ¿Cuánto desea retirar?");
		    Scanner disminuir = new Scanner(System.in);
		    int opcion_disminuir = disminuir.nextInt();
		    int saldo_final = cliente.getSaldo() - opcion_disminuir;
		    if (opcion_disminuir > cliente.getSaldo()) {
			System.out.println(
				"\r El monto ingresado excede al saldo que posee en su cuenta. Favor, intente nuevamente");
			break;
		    } else if (opcion_disminuir <= cliente.getSaldo()) {
			System.out.println("\n> Se ha descontado: " + opcion_disminuir + " CLP de su cuenta.");
			System.out.println("\r\n > Su nuevo saldo es de: " + saldo_final + " CLP.");
			System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			break;
		    }

		}
		// Si el usuario ingresa una tecla o caracter no válido
		else {
		    System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
		}

		break;
	    // El usuario ingresa a Tarjeta de Crédito.
	    case 2:
		System.out.println("\r\n\t***** Ha ingresado exitosamente a Tarjeta de Crédito. *****");
		System.out.println("\n ~ Su saldo es de: " + cliente.getSaldo() + " CLP. ~");
		System.out.println(
			"\r\n Línea de Crédito habilitada. ¿Qué desea hacer? (Disponibilidad hasta 100.000 CLP).");
		System.out.println("\t[1] Aceptar el avance.");
		System.out.println("\t[2] Declinar el avance.");
		Scanner linea_credito = new Scanner(System.in);
		int decision = linea_credito.nextInt();

		/*
		 * Se genera un switch que presenta dos posibles escenarios: 1.- El usuario
		 * acepta tomar el crédito, además de poder manipular el monto. 2.- El usuario
		 * declinar tomar el crédito, manteniendo la posibilidad de manipular el monto.
		 */
		switch (decision) {
		// El usuario acepta tomar el crédito ofrecido por el banco
		case 1:
		    System.out.println(
			    "\r\n> Ingrese cantidad de crédito que desea tomar (Disponibilidad hasta 100.000 CLP.):");
		    Scanner solicitud = new Scanner(System.in);
		    int credito_escogido = solicitud.nextInt();
		    if (credito_escogido <= 100000) {
			System.out.println("\r\n\t¡Su línea de crédito ha sido aprobada!");
			int new_saldo = cliente.getSaldo() + credito_escogido;
			System.out.println("\n > Su nuevo saldo es de: " + new_saldo + " CLP.");
			System.out.println("\n> ¿Qué desea hacer?");
			System.out.println("\t[1] Depositar dinero.");
			System.out.println("\t[2] Retirar dinero.");
			Scanner depositar_retirar = new Scanner(System.in);
			int escoger = depositar_retirar.nextInt();
			// Usuario aumenta el saldo inicial
			if (escoger == 1) {
			    System.out.println("\n> ¿Cuánto desea depositar?");
			    Scanner aumentar_saldo = new Scanner(System.in);
			    int sumar_final = aumentar_saldo.nextInt();
			    int resultado_saldo = new_saldo + sumar_final;
			    System.out.println("\r\n > Su nuevo saldo es de: " + resultado_saldo + " CLP.");
			    System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			    break;
			}
			// Usuario resta al saldo inicial
			else if (escoger == 2) {
			    System.out.println("\n> ¿Cuánto desea retirar?");
			    Scanner disminuir_saldo = new Scanner(System.in);
			    int resta_final = disminuir_saldo.nextInt();
			    int resultado_final_r = new_saldo - resta_final;
			    // Usuario ingresa monto mayor al posible
			    if (resta_final > new_saldo) {
				System.out.println(
					"\r El monto ingresado excede al saldo que posee en su cuenta. Favor, intente nuevamente");
				break;
			    }
			    // Usuario ingresa monto igual o menor al posible
			    else if (resta_final <= new_saldo) {
				System.out.println("\n> Se ha descontado de su cuenta: " + resta_final + " CLP.");
				System.out.println("\r\n > Su nuevo saldo es de: " + resultado_final_r + " CLP.");
				System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
				break;
			    }

			}

			break;
			// Usuario excede el monto ofrecido crediticio
		    } else if (credito_escogido > 100000) {
			System.out.println(
				"\r\n Ha ingresado un monto superior al disponible. Favor, intente nuevamente");
			break;
		    }
		    // En caso de que usuario decline tomar el crédito
		case 2:
		    System.out.println("\r\n\tHa declinado tomar línea de crédito ofrecida");
		    System.out.println("\n > Su saldo es de: " + cliente.getSaldo() + " CLP.");
		    System.out.println("\n> ¿Qué desea hacer?");
		    System.out.println("\t[1] Depositar dinero.");
		    System.out.println("\t[2] Retirar dinero.");
		    Scanner saldo_definicion = new Scanner(System.in);
		    int diseño_saldo = saldo_definicion.nextInt();
		    // Usuario aumenta el monto inicial
		    if (diseño_saldo == 1) {
			System.out.println("\n> ¿Cuánto desea depositar?: ");
			Scanner totalidad = new Scanner(System.in);
			int total_tarjeta = totalidad.nextInt();
			int resultado_saldo_tarjeta = cliente.getSaldo() + total_tarjeta;
			System.out.println("\r\n > Su nuevo saldo es de: " + resultado_saldo_tarjeta + " CLP.");
			System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			break;

		    }
		    // Usuario resta al saldo inicial
		    if (diseño_saldo == 2) {
			System.out.println("\n> ¿Cuánto desea retirar?: ");
			Scanner totalidad_r = new Scanner(System.in);
			int resultado_saldo_tarjeta_r = totalidad_r.nextInt();
			int saldo_final_tarjeta = cliente.getSaldo() - resultado_saldo_tarjeta_r;
			if (resultado_saldo_tarjeta_r > cliente.getSaldo()) {
			    System.out.println(
				    "\r El monto ingresado excede al saldo que posee en su cuenta. Favor, intente nuevamente");
			    break;
			} else if (resultado_saldo_tarjeta_r <= cliente.getSaldo()) {
			    System.out.println(
				    "\n> Se ha descontado de su cuenta: " + resultado_saldo_tarjeta_r + " CLP.");
			    System.out.println("\r\n > Su nuevo saldo es de: " + saldo_final_tarjeta + " CLP.");
			    System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			    break;
			}
		    }
		    break;
		// Si usuario pulsa una tecla o caracter no considerado en las opciones
		default:
		    System.out.print("\r\n\t *** El dato ingresado no es válido. Intente nuevamente. ***");
		}

		break;
	    // El usuario ingresa a Tarjeta de Débito
	    case 3:
		System.out.println("\r\n\t***** Ha ingresado exitosamente a Tarjeta de Débito. *****");
		System.out.println("\n ~ Su saldo es de: " + cliente.getSaldo() + " CLP. ~");
		System.out.println("\n> ¿Qué desea hacer?");
		System.out.println("\t[1] Depositar dinero.");
		System.out.println("\t[2] Retirar dinero.");
		Scanner debito_final = new Scanner(System.in);
		int debito_saldo = debito_final.nextInt();
		// Usuario aumenta el saldo inicial
		if (debito_saldo == 1) {
		    System.out.println("\n> ¿Cuánto desea depositar?: ");
		    Scanner debito_depositar = new Scanner(System.in);
		    int debito_final_saldo = debito_depositar.nextInt();
		    int debito_resultado = cliente.getSaldo() + debito_final_saldo;
		    System.out.println("\r\n > Su nuevo saldo es de: " + debito_resultado + " CLP.");
		    System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
		    break;
		}
		// Usuario resta al saldo inicial
		else if (debito_saldo == 2) {
		    System.out.println("\n> ¿Cuánto desea retirar?: ");
		    Scanner resta_debito = new Scanner(System.in);
		    int resta_debito_final = resta_debito.nextInt();
		    int resultado_resta_debito = cliente.getSaldo() - resta_debito_final;
		    if (resta_debito_final > cliente.getSaldo()) {
			System.out.println(
				"\r El monto ingresado excede al saldo que posee en su cuenta. Favor, intente nuevamente");
			break;
		    } else if (resta_debito_final <= cliente.getSaldo()) {
			System.out.println("\n> Se ha descontado de su cuenta: " + resta_debito_final + " CLP.");
			System.out.println("\r\n > Su nuevo saldo es de: " + resultado_resta_debito + " CLP.");
			System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			break;
		    }
		}
		break;

	    // Caso de no presionar los numeros indicados
	    default:
		System.out.print("\r\n\t *** El dato ingresado no es válido. Intente nuevamente. ***");

	    }

	}

    }
}
// Cierre de la clase
