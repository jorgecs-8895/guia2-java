# Programación Avanzada con Java
La siguiente guía consiste en una serie de cuatro ejercicios, orientados al uso de métodos, clases, encapsulamiento, ArraysList, Setters & Getters, entre otras características propias del lenguaje Orientado a Objetos (POO), utilizando para ello el lenguaje de programación Java


# Historia
Este trabajo fue escrito y desarrollado en conjunto por los autores Jorge Carrillo Silva y Nicolás Sepúlveda Fálcon, utilizando para ello el lenguaje de programación Java (openjdk 14.0.2).
Los ejercicios siguen el estándar de Google Style Guides (https://github.com/google/styleguide.git).
Se han importado librerías, luna de las cuales es Scanner. 


# Para empezar:
Es requisito tener instalado en su computadora Java, de preferencia las últimas versiones de openjdk (14.0.2) y su ejecución está pensada para entornos Linux.


# Instalación:
Para instalar y ejecutar el programa en su máquina, es necesario que siga las presentes indicaciones:
1- Clonar el repositorio "Programación Avanzada con Java" en el directorio de su preferencia, el enlace HTTPS es https://gitlab.com/jorgecs-8895/guia2-java.git
2- Luego, deberá entrar en la carpeta clonada que lleva por nombre "Guía 2" que contiene 4 carpetas, es esencial que pueda comprobar y cerciorarse que se encuentran los paquetes y librerías esenciales, ya que de lo contrario los proyectos podrían no funcionar adecuadamente (Todos los Ej, además de guia2.pdf, Pauta guía2.pdf y README.md)
3- Posteriormente, ya está usted preparado para ejecutar el programa.


# Codificación:
Los proyectos fueron construidos con el estándar de codificación UTF-8.


# Construido con:
Eclipse: Los proyectos se desarrollaron de manera exclusiva con este IDE.


# Licencia:
Este proyecto está sujeto bajo la Licencia GNU GPL v3.


# Autores y creadores de los proyectos:
Jorge Carrillo Silva y Nicolás Sepúlveda Fálcon.
 
