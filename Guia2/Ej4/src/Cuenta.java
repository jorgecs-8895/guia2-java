
// Importación de librería(s) y/o paquetes requeridos para el correcto funcionamiento del programa
import java.util.Scanner;

@SuppressWarnings("unused")
public class Cuenta {

    @SuppressWarnings("resource")
    public static void main(String[] args) {
	// TODO Esbozo de método generado automáticamente

	Scanner scanner = new Scanner(System.in);
	String nombre;
	String bankname;
	int menu;
	double clave;
	double saldo;
	double nrocta;
	double deposito;
	double retiro;
	double transferencia;

	// Creación y generación de constructor
	Controladora cliente = new Controladora();

	/*
	 * LINEAS 33 - 59. En esta sección de código se establecen los parámetros
	 * iniciales de una cuenta bancaria (nombre del cliente, nombre del banco,
	 * número de cuenta y el saldo (inicial) de la cuenta.)
	 * 
	 */

	System.out.println("\r\n\t\t ** Se ha iniciado una nueva sesión. **");

	System.out.println("\n\t Bienvenid@ a Transbank.");

	System.out.println("\n> Ingrese nombre de banco del cual es cliente: ");
	bankname = scanner.nextLine();
	cliente.setBankname(bankname);

	System.out.println("\n> Ingrese nombres y apellidos: ");
	nombre = scanner.nextLine();
	cliente.setNombre(nombre);

	System.out.println("\n> Ingrese su número de cuenta: ");
	nrocta = scanner.nextDouble();
	cliente.setNrocta(nrocta);

	System.out.println("\n> Por último, ingrese su clave secreta: ");
	clave = scanner.nextDouble();
	cliente.setClave(clave);

	System.out.println("\r\n\t*** Ha accedido exitosamente a su cuenta. ***");
	System.out.println("\r\n> Bienvenid@ a " + cliente.getBankname() + ", don(a): " + cliente.getNombre() + ".");

	System.out.println("\r\n> Por favor, ingrese su saldo en CLP: ");
	saldo = scanner.nextInt();
	if (saldo >= 0 && saldo <= 999999999) {

	    cliente.setSaldo(saldo);

	    System.out.println("\n> ¿Qué desea hacer?");
	    System.out.println("\t[1] Depositar dinero.");
	    System.out.println("\t[2] Retirar dinero.");
	    System.out.println("\t[3] Transferir dinero");

	    menu = scanner.nextInt();

	    if (menu >= 1 && menu <= 3) {
		cliente.setMenu(menu);

		switch (menu) {
		// Opción 1: El usuario decide depositar dinero a su cuenta
		case 1:
		    if (menu == 1) {

			double saldototal;

			System.out.println("\n> ¿Cuánto desea depositar?");
			deposito = scanner.nextDouble();
			if (deposito >= 0) {
			    saldototal = cliente.getSaldo() + deposito;
			    System.out.println("\r\n > Su nuevo saldo es de: " + saldototal + " CLP.");
			    System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
			} else {
			    System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
			    System.exit(0);
			}
		    }
		    break;

		// Opción 2: El usuario decide retirar dinero de su cuenta
		case 2:
		    if (menu == 2) {

			double saldototal;

			System.out.println("\n> ¿Cuánto desea retirar?");
			retiro = scanner.nextDouble();
			if (retiro >= 0) {
			    saldototal = cliente.getSaldo() - retiro;
			    if (retiro <= cliente.getSaldo()) {
				System.out.println("\n> Se ha descontado de su cuenta: " + retiro + " CLP.");
				System.out.println("\r\n > Su nuevo saldo es de: " + saldototal + " CLP.");
				System.out.println("\r\n\t ***** GRACIAS POR SU PREFERENCIA *****");
				break;

			    } else {
				System.out.println(
					"\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
				System.exit(0);

			    }

			} else {
			    System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
			    System.exit(0);
			}

		    }

		    break;

		// Opción 3: El usuario decide transferir dinero a otra cuenta
		case 3:
		    if (menu == 3) {

		    }
		    break;
		}

	    } else {
		System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
		System.exit(0);
	    }

	} else {
	    System.out.println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***");
	    System.exit(0);
	}

    }

}
