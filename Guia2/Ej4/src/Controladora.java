public class Controladora {

    private String nombre;
    private String bankname;
    private double clave;
    private double saldo;
    private double nrocta;
    private double menu;

    // Controladora cliente = new Controladora();

    // Constructor por defecto
    public Controladora() {

    }

    // Constructor con parámetros
    public Controladora(String nombre, String bankname, double clave, double saldo, double nrocta) {
	this.nombre = nombre;
	this.bankname = bankname;
	this.clave = clave;
	this.saldo = saldo;
	this.nrocta = nrocta;
    }

    /* GETTER'S */

    // Get Nombre
    public String getNombre() {

	return this.nombre;
    }

    // Get Nombre Banco
    public String getBankname() {

	return this.bankname;

    }

    // Get Clave
    public double getClave() {

	return this.clave;

    }

    // Get Saldo
    public double getSaldo() {

	return this.saldo;

    }

    // Get Numero de cuenta
    public double getNrocta() {

	return this.nrocta;
    }

    public double getMenu() {
	/*
	 * Scanner scanner = new Scanner(System.in); int menu = scanner.nextInt(); if
	 * (menu >= 1 && menu <= 3) { cliente.setMenu(menu);
	 * System.out.println("\n> ¿Qué desea hacer?");
	 * System.out.println("\t[1] Depositar dinero.");s
	 * System.out.println("\t[2] Retirar dinero.");
	 * System.out.println("\t[3] Transferir dinero");
	 * 
	 * switch (menu) { case 1: break; case 2: break; case 3: break; }
	 * 
	 * } else System.out.
	 * println("\r\n\t\t *** Valor ingresado inválido. Favor intente nuevamente. ***"
	 * ); System.exit(0);
	 */
	return this.menu;
    }

    /* SETTER'S */

    // Set Nombre
    public void setNombre(String N) {

	this.nombre = N;

    }

    // Set nombre Banco
    public void setBankname(String NB) {

	this.bankname = NB;
    }

    // Set Clave
    public void setClave(double C) {

	this.clave = C;
    }

    // Set Saldo
    public void setSaldo(double S) {

	this.saldo = S;

    }

    // Set Numero de cuenta
    public void setNrocta(double NC) {

	this.nrocta = NC;
    }

    // Set Menu
    public void setMenu(double M) {

	this.menu = M;
    }

}
